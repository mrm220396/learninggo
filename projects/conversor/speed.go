package conversor

// Speed returns the speed of an object in Float 32
func Speed(distance, time float32) float32 {
	return distance / time
}
