package main

import "fmt"

var x bool

func main() {
	fmt.Println(x)
	x = true
	fmt.Println(x)
	x = 10 == 10
	fmt.Println(x)
	x = 10 != 10
	fmt.Println(x)
	x = 10 <= 1
	fmt.Println(x)
	x = 1 > 10
	fmt.Println(x)
	x = 9 < 10
	fmt.Println(x)
}
