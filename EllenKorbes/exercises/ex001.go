// Jan Mesu 05/23/2020
// Online resource https://play.golang.com/p/38NXpQBVtFs

package main

import (
	"fmt"
)

func main() {
	numero := 10
	fmt.Printf("%d\t%b\t\t%x", numero, numero, numero)
}
